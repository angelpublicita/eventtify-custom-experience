<?php

//PROGRESS FORM
function progress_form_func(){
	?>
		<div class="container-form-progress">
			<form action="" class="form-register" method="GET">
				<div class="form-register-header">
					<ul class="progressbar">
						<li class="progressbar-option active active"></li>
						<li class="progressbar-option"></li>
						<li class="progressbar-option"></li>
					</ul>
				</div>
				<div class="form-register-body">
					<div class="step active" id="step-1">
						<div class="step-header">
						<h2 class="step-title">Información general</h2>
						</div>
						<div class="step-body row">
						<div class="form-group col-12 col-md-6">
							<label for="pf-name">Nombres</label>
							<input type="text" id="pf-name" class="form-control">
						</div>
						<div class="form-group col-12 col-md-6">
							<label for="pf-email">Correo</label>
							<input type="email" id="pf-email" class="form-control">
						</div>
						<div class="form-group col-12 col-md-6">
							<label for="pf-phone">Celular</label>
							<input type="number" id="pf-phone" class="form-control">
						</div>
						<div class="form-group col-12 col-md-6">
							<label for="pf-city">Ciudad</label>
							<select name="pf-city" id="pf-city" class="form-control custom-select">
								<option selected disabled>Selecciona una opción</option>
								<option value="cartagena">Cartagena</option>
								<option value="barranquilla">Barranquilla</option>
							</select>
						</div>
						<div class="step-footer">
							<button type="button" class="step-button step-button-next" data-to_step="2" data-step="1">SIGUIENTE</button>
						</div>
						</div>
					</div>
					<div class="step" id="step-2">
						<div class="step-header">
						<h2 class="step-title">Información del evento</h2>
						</div>
						<div class="step-body row">
						<div class="form-group col-12 col-md-6">
							<label for="pf-date">Fecha del evento</label>
							<input type="text" id="pf-date" class="form-control">
						</div>
						<div class="form-group col-12 col-md-6">
							<label for="pf-cant">Cantidad de personas</label>
							<input type="number" id="pf-cant" min="1" max="30" class="form-control">
						</div>
						<div class="form-group group-time col-12 col-md-6">
							<label for="pf-datetime-init">Hora Inicio</label>
							<input type="number" min="1" max="12" id="pf-datetime-init" class="form-control">

							<div class="inputs-check">
								<div class="form-check form-check-inline">
									<input class="form-check-input" type="radio" name="hora-inicio-am" id="hora-inicio-am" value="option1">
									<label class="form-check-label" for="inlineRadio1">am</label>
								</div>
								<div class="form-check form-check-inline">
									<input class="form-check-input" type="radio" name="hora-inicio-pm" id="hora-inicio-pm" value="option2">
									<label class="form-check-label" for="inlineRadio2">pm</label>
								</div>
							</div>
						</div>
						<div class="form-group group-time col-12 col-md-6">
							<label for="pf-datetime-close">Hora cierre</label>
							<input type="number" min="1" max="12" id="pf-datetime-close" class="form-control">

							<div class="inputs-check">
								<div class="form-check form-check-inline">
									<input class="form-check-input" type="radio" name="hora-fin-am" id="hora-fin-am" value="option1">
									<label class="form-check-label" for="inlineRadio1">am</label>
									</div>
									<div class="form-check form-check-inline">
									<input class="form-check-input" type="radio" name="hora-fin-pm" id="hora-fin-pm" value="option2">
									<label class="form-check-label" for="inlineRadio2">pm</label>
								</div>
							</div>
						</div>
						<div class="step-footer">
							<button type="button" class="step-button step-button-back" data-to_step="1" data-step="2">ANTERIOR</button>
							<button type="button" class="step-button step-button-next" data-to_step="3" data-step="2">SIGUIENTE</button>
						</div>
						</div>
					</div>
					<div class="step" id="step-3">
						<div class="step-header">
						<h2 class="step-title">Información del evento</h2>
						</div>
						<div class="step-body row">
						<div class="form-group col-12">
							<label for="pf-event-type">Tipo de evento</label>
							<select class="form-control" id="pf-event-type">
								<option selected disabled>Selecciona una opción</option>
								<option value="reunion-familiar">Reunión Familiar</option>
								<option value="reunion-de-amigos">Reunión de amigos</option>
								<option value="cena-romantica">Cena romántica</option>
								<option value="pedida-de-mano">Pedida de mano</option>
								<option value="bautizo">Bautizo</option>
								<option value="brunch">Brunch</option>
								<option value="cumpleanos-infantil">Cumpleaños Infantil</option>
								<option value="reunion-infantil">Reunión Infantil</option>
								<option value="cumpleanos-adulto">Cunpleaños Adulto</option>
							</select>
						</div>
						<div class="form-group col-12 col-md-6">
							<label for="pf-event-class">Clase de evento</label>
							<select class="form-control" id="pf-event-class">
								<option selected disabled>Selecciona una opción</option>
								<option value="formal">Formal</option>
								<option value="informal">Informal</option>
							</select>
						</div>
						<div class="form-group col-12 col-md-6">
							<label for="pf-event-location">Lugar</label>
							<select class="form-control" id="pf-event-location">
								<option selected disabled>Selecciona una opción</option>
								<option value="mi-casa">Mi casa</option>
								<option value="salon">Zona social salón</option>
								<option value="piscina">Zona social piscina</option>
							</select>
						</div>
						<div class="step-footer">
							<button type="button" class="step-button step-button-back" data-to_step="2" data-step="3">ANTERIOR</button>
							<button type="submit" class="">FINALIZAR</button>
						</div>
						</div>
					</div>
				</div>
			</form>
		</div>
	<?php
}

add_shortcode("progress_form","progress_form_func");

// //ADD LINK TO BUTTON MENU
// add_filter( 'nav_menu_link_attributes', 'wpse121123_contact_menu_atts', 10, 3 );
// function wpse121123_contact_menu_atts( $atts, $item, $args )
// {
//   // The ID of the target menu item
//   $menu_target = 14;

//   // inspect $item
//   if ($item->ID == $menu_target) {
// 	$atts['data-toggle'] = 'modal';
// 	$atts['data-target'] = '#modalFormProgress';
//   }
//   return $atts;
// }