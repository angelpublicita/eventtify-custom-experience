<?php
/*
Plugin Name: Eventtify Custom Experience
Plugin URI: https://www.geniorama.site/
Description: Plugin desarrollado para personalizar la experiencia en creación de eventos
Version: 1.0
Author: Angel Burgos
Author URI: https://www.instagram.com/angelweblover/
License: GPL
Text Domain: eventtify-custom-experience
Domain Path: /languages/
*/

if ( ! defined( 'ABSPATH' ) ) {
	die();
}

include plugin_dir_path(__FILE__ ) . "includes/progress-form.php";